<?php $this->load->view('admin/header'); ?>

<div class="row">
<div class="col-md-3">
<div class="list-group">
  <a href="<?= URL_ROOT_ACTION.'admin' ?>"  class="list-group-item active">
    Dashboard
  </a>
 <a href="<?= URL_ROOT_ACTION.'blog/addblog' ?>" class="list-group-item">Register customers
  </a>
  <a href="<?= URL_ROOT_ACTION.'blog/listblog' ?>" class="list-group-item">Record Transaction
  </a>
  <a href="<?= URL_ROOT_ACTION.'blog/charttrans' ?>" class="list-group-item">Transaction Chart
  </a>
</div>
</div>

<div class="col-md-9">
<?php
if($error = $this->session->flashdata('blogerror'))
echo $error;

?>
<h2>List/View of All Registered Customers</h2>
<table class="table  table-hover ">
  <thead>
    <tr class="info">
      <th>#</th>
      <th>Title</th>
      <th>Content </th>
      <th width="5%">View</th>
    </tr>
  </thead>
  <tbody>
  
  <?php
if(count($blogs)>0)
{

$counter=$this->uri->segment(4)*$this->pagination->per_page - $this->pagination->per_page ;
if($counter<0)
$counter = 0;
foreach($blogs as $blog)
{
  
echo '<tr>
      <td>'.++$counter.'</td>
      <td>'.$blog['title'].'</td>
      <td>'.$blog['body'].'</td>
      <td class="text-center">
	  <a href="'.URL_ROOT_ACTION.'blog/editblog/'.$blog['id'] .'" class="btn btn-primary btn-block">Edit</a> 
	  <a onclick="return confirm(\'Are you sure You want to delete this record\');" href="'.URL_ROOT_ACTION.'blog/deleteblog/'.$blog['id'].'" class="btn btn-danger btn-block">Delete</a>
	
	  </td>
    </tr>';
	
}

}else
{
echo '<tr>
      <td colspan="4" class="text-center">No Record Found</td>
      
    </tr>';
}
?>
  </tbody>
</table> 
<div class="text-center">
<?= $this->pagination->create_links() ?>
</div>
</div>
</div>

<?php $this->load->view('admin/footer');?>