<?php $this->load->view('admin/header'); ?>

<div class="row">
<div class="col-md-3">
<div class="list-group">
  <a href="<?= URL_ROOT_ACTION.'admin' ?>"  class="list-group-item active">
    Dashboard
  </a>
  <a href="<?= URL_ROOT_ACTION.'blog/addblog' ?>" class="list-group-item">Register customers
  </a>
  <a href="<?= URL_ROOT_ACTION_ACTION.'' ?>" class="list-group-item">Transaction Chart
  </a>
 



  
</div>
</div>

<div class="col-md-9">
  <?php
if($error = $this->session->flashdata('blogerror'))
echo $error;

 echo form_open("admin/blog/save_blog",["class"=>"form-horizontal","role"=>"form"]); ?>
  <fieldset>
    <legend>Customers Registration</legend>
    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Customer's Name</label>
      <div class="col-lg-10">
      <?php echo form_input(["type"=>"text", "class"=>"form-control" ,"id"=>"title", "placeholder"=>"Full Name","name"=>"title","value"=>set_value('title')]) ,form_error("title") ;?>
        
      </div>
    </div>
<!--Newly added iNformation- ------>




  
    <div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Phone Contact</label>
      <div class="col-lg-10">
      <?php echo form_input(["class"=>"form-control" ,"id"=>"body", "placeholder"=>"Please Supply your Mobile No..","name"=>"body",
      "row"=>"3","value"=>set_value('body')]) , form_error("body") ;?>
       
        
      </div>
    </div>
      


    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
      <?php echo form_reset(["class"=>"btn btn-default","value"=>"Cancel"]) , form_submit(["class"=>"btn btn-primary","value"=>"submit"]) ;?>
       
      </div>
    </div>
  </fieldset>
</form>


</div>
</div>



<?php $this->load->view('admin/footer');?>