<?php $this->load->view('common/header'); ?>

<h3> <?= $blog->title ?></h3>
  <blockquote >
    <p><?= $blog->body ?></p>
    <footer >Writer : <?= $blog->fname ?> <br/ > <?= date("d M Y",strtotime($blog->created_datetime)) ?></footer>
  </blockquote>
<?php $this->load->view('common/footer');?>